import React from 'react'
import Phone from '../assets/Content/banner/iPhone7Cropped.png'

const Banner = () => {

  return(
    <div className="banner-container">
      <div className="banner-img-container">
        <img src={Phone} className="banner-img" />
      </div>
      <div className="banner-content">
        <h1 className="section-title banner-title">The Only Platform You'll Need to Run Smart Coworking Spaces & Serviced Offices</h1>
        <div className="content-text">
          Oqulo is built to sell, manage and grow your commercial real estate business. 
          Collect payments, manage clients and run reports using our booking app. 
          Engage members using our community messaging feature. 
          <p/>
          Be the first in line to take Oqulo for a test drive!
        </div>
      </div>
      <div className="banner-inputs">
        <div>
          <input id="banner-input" placeholder="Email address" />
        </div>
        <div>
          <button id="banner-button">NOTIFY ME</button>
        </div>
        <div className="banner-input-subtxt">
          *No spam, thats a promise.
      </div>
      </div>
    </div>
  )

}

export default Banner;