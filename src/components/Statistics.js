import React from 'react'
import Graph from '../assets/Content/statistics/Graph.png'

const Statistics = () => {

  return(
    <div className="statistics-container">
      <div className="statistics-grid-container">
        <div className="statistics-title">
          <h1 className="section-title">Stats Delivered Beatifully</h1>

          <div className="content-text">
            View sales charts, booking ratio and user behavior using Oqulo's visual reporting feature.
          </div>
        </div>
        <div className="statistics-image">
          <img id="statistics-image" src={Graph} />
        </div>
      </div>
    </div>
  )

}

export default Statistics;