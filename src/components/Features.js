import React from 'react'
import Image from '../assets/Content/features/Screen.png'

import FImg1 from '../assets/Content/features/fingerscreen.png'
import FImg2 from '../assets/Content/features/mobileapp.png'
import FImg3 from '../assets/Content/features/handphone.png'
import FImg4 from '../assets/Content/features/phoneblocked.png'

const Features = () => {

  return (
    <div className="features-container">
      <div className="features-grid-container">
        <div className="features-title">
          <h1 className="section-title">Oqulo Features at a Glance</h1>

          <div className="content-text">
            Powerful functionalities that changes the way you do business.
          </div>
        </div>
        <div className="features-content">
          <div className="feature fc-1">
            <h4 style={{ margin: `0 0 10px 0` }}>Powerful Space Management</h4>

            Manage meeting room and desk bookings, create events, sell tickets, schedule private office showings, automate invoicing and connect with members --- all in one central dashboard.
          </div>
          <div className="feature-img fi-1">
            <img src={FImg1} />
          </div>
          <div className="feature-main-image">
            <img id="feature-img" src={Image} />
          </div>
          <div className="feature-img fi-2">
            <img src={FImg2} />
          </div>
          <div className="feature fc-2">
            <h4 style={{ margin: `0 0 10px 0` }}>User Friendly Interface</h4>

            Clients will find it easy tobook and pay for their space, thanks to Oqulo's easy navigation and pixel-perfect design. Keep members up to date with Oqulo's community board and help desk features.
          </div>
          <div className="feature fc-3">
            <h4 style={{ margin: `0 0 10px 0` }}>Painless Integration</h4>

            No matter what your website is built on, Oqulo is easy to setup and integrate with CRM and payment gateways. Go live in a matter of days.
          </div>
          <div className="feature-img fi-3">
            <img src={FImg3} />
          </div>
          <div className="feature-img fi-4">
            <img src={FImg4} />
          </div>
          <div className="feature fc-4">
            <h4 style={{ margin: `0 0 10px 0` }}>Secure Data & White Label Branding</h4>

            Get peace of mind in knowing that client information and sales data are stored in a secure server. Our white label service allows you to market this platform as your own.
          </div>
        </div>
      </div>
    </div>
  )

  /* CSS FLEXBOX LAYOUT

  return(
    <div className="features-container content">
      <div className="features-title">
        TITLE -- FEATURES
      </div>
      <div className="features-content">
        <div className="features-group-1">
          <div className="feature">
            FEATURE 1
          </div>
          <div className="feature">
            FEATURE 2
          </div>
        </div>
        <div className="features-image">
          IMAGE
        </div>
        <div className="features-group-2">
          <div className="feature">
            FEATURE 3
          </div>
          <div className="feature">
            FEATURE 4
          </div>
        </div>
      </div>
    </div>
  )

  */

}

export default Features;