import React from 'react'
import Image from '../assets/Content/intro/IntroImage.png'

const Intro = () => {

  return(
    <div className="intro-container">
      <div className="intro-grid-container">
        <div className="intro-title">
          <h1>Tried & Tested Space Management Software</h1>
        </div>
        <div className="intro-image">
          <img id="intro-image" src={Image} />
        </div>
        <div className="intro-text content-text">
          Oqulo is a homegrown app that's been tested by real-life businesses. 
          Whether you operate on a single building or in multiple ocations. 
          Oqulo is designed to make your space leasing operations hassle-free.
          <p/>
          Your clients will have a smooth booking & online payment experiene, 
          and your concierge staff will be able to view occupancy stats and 
          generate reports at a click of a button.
        </div>
      </div>
    </div>
  )

}

export default Intro;