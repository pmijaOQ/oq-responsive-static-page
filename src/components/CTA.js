import React from 'react'

const CTA = () => {

  return(
    <div className="cta-container">
      <div className="cta-grid-container">
        <div className="cta-title">
          <h1 style={{ margin: `0 0 20px 0` }}>Launching Soon</h1>

          Sign up to get updates on Oqulo's public release.
        </div>
        <div className="cta-input">
          <input id="cta-input" placeholder="Email address" />
          <div className="cta-button">
            <button id="cta-button" onClick={() => {}}>TRY THE BETA</button>
          </div>
        </div>
      </div>
    </div>
  )

}

export default CTA;