import React from 'react'
import logo from '../assets/Footer/OquloLogo.png'
import SocMed from '../assets/Footer/SocialMediaIcons.png'

const MyFooter = () => {

  return(
    <div className="footer-container">
      <div className="footer-grid-container">
        <div className="footer-img">
          <img id="footer-img" src={logo} />
        </div>
        <div className="footer-nav">
          <div className="footer-nav-item">
            <a className="footer-link" href={"#"}>DISCOVER OQULO</a>
          </div>
          <div className="footer-nav-item">
            <a className="footer-link" href={"#"}>FEATURES</a>
          </div>
          <div className="footer-nav-item">
            <a className="footer-link" href={"#"}>CONTACT</a>
          </div>
        </div>
        <div className="footer-soc-med">
          <img src={SocMed} />
        </div>
        <div className="footer-copyright">
          Copyright @ Oqulo 2018. All rights reserved.
        </div>
      </div>
    </div>
  )

}

export default MyFooter;