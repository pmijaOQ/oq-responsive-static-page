import React from 'react'
import { slide as Menu } from 'react-burger-menu'

import logo from '../assets/Header/OquloLogo.png'
import burger from '../assets/Header/burger.png'

const MyHeader = () => {

  const [navOpen, setNavOpen] = React.useState(false)

  const handleOpen = () => {
    setNavOpen((prevState) => { return !prevState })
  }

  const handleClose = () => {
    setNavOpen(false)
  }

  return(
    <div>
      <Menu isOpen={navOpen} onClose={handleClose} width={"230px"} right>
        <a className="footer-link" href={"#"}>DISCOVER OQULO</a>
        <a className="footer-link" href={"#"}>FEATURES</a>
        <a className="footer-link" href={"#"}>CONTACT</a>
      </Menu>
      
      <div className="header-container">
        <div className="header-logo-container">
          <img id="header-logo" src={logo} className="header-logo" />
        </div>
        <div className="header-nav">
          <div className="nav-burger" onClick={handleOpen}>
            <img className="nav-burger" src={burger} />
          </div>
          <div className="nav-item"><a href={"#"}>DISCOVER OQULO</a></div>
          <div className="nav-item"><a href={"#"}>FEATURES</a></div>
          <div className="nav-item"><a href={"#"}>CONTACT</a></div>
        </div>
      </div>
    </div>
  )

}

export default MyHeader;