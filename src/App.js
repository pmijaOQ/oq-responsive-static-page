import MyFooter from './components/MyFooter'
import Banner from './components/Banner'
import Intro from './components/Intro'
import Features from './components/Features'
import Statistics from './components/Statistics'
import CTA from './components/CTA'
import MyHeader from './components/MyHeader'

import { slide as Menu } from 'react-burger-menu'
import './sccs/App.scss'

function App() {
  return (
    <div>
      <div className="Content-container">
        <div id="header-bg">
          <div className="content">
            <MyHeader />
            <Banner />
          </div>
        </div>
        <div className="content">
          <Intro />
        </div>
        <div id="features-bg">
          <div className="content">
            <Features />
          </div>
        </div>
        <div className="content">
          <Statistics />
        </div>
        <div id="cta-bg">
          <div className="content">
            <CTA />
          </div>
        </div>
        <div id="footer-bg">
          <div className="content">
            <MyFooter />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
